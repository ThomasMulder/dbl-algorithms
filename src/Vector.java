public class Vector {
	float x;
	float y;
	public Vector(float a, float b){
		x = a;
		y = b;
	}
	public Vector(Point a, Point b){
		x = a.x - b.x;
		y = a.y - b.y;
	}
	
	public float length(){
		return (float)Math.sqrt(x*x + y*y);
	}
	
}
