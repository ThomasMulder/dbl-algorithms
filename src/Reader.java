
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s124392
 */
public class Reader {

    public static ArrayList<Point> points = new ArrayList<Point>();
    public static boolean multiple;
    public static boolean curve;
    public static boolean network;
    public static boolean peach = true;
    public static void read(){
        try
        {	Scanner s;
        	if(!peach){s = new Scanner(new File("input.txt"));}
        	else{s = new Scanner(System.in);}
            s.next();
            String type = s.next();
            if(type.equals("multiple") || type.equals("single")){
            	curve = true;
            	if(type.equals("multiple")){multiple = true;}
            	else{multiple=false;}
            }else{
            	network = true;
            }
            
            
            s.nextLine();
            s.nextLine();
            while(s.hasNextLine()){
                int id = s.nextInt();
                float x = Float.valueOf(s.next());
                float y = Float.valueOf(s.next());
                points.add(new Point(id,x,y));
            }
        }
       
        
        
        
        catch(Exception e){e.printStackTrace();}
    }
}
