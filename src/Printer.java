
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s124392
 */
public class Printer {
    public static void print(ArrayList<Line> l, ArrayList<Point> p, boolean network, boolean peach)
    {
	      if(!peach)
	      {
	    	  try
	    	  {
	            File fin = new File("input.txt");
	            File fout = new File("output.txt");
	            fout.createNewFile();
	            
	            FileReader in = new FileReader(fin);
	            FileWriter out = new FileWriter(fout);
	            
	            int c = 0;
	            while ((c = in.read()) != -1){out.write(c);}
	            if(network){
	                out.write("\r\nm "+p.size());
	                for(Point point: p){
	                    out.write("\r\n"+point.id+" "+point.x+" "+point.y);
	                }
	            }
	            out.write("\r\ns "+l.size());
	            for(Line line: l){
	                out.write("\r\n" + line.p1.id + " " + line.p2.id );
	            }
	            
	 
	            in.close();
	            out.close();
	            
	    	  }
	    	  catch(Exception e){e.printStackTrace();}
	      }
	      else
	      {
	    	try{
		    	File fin = new File("input.txt");
		        
		        BufferedReader in = new BufferedReader(new FileReader(fin));
		        
		        while (in.ready()){System.out.println(in.readLine());}
		        if(network){
		            System.out.println(p.size());
		            for(Point point: p){
		               	System.out.println(point.id+" "+point.x+" "+point.y);
		            }
		        }
		        System.out.println(l.size()+" number of segments");
		        for(Line line: l){
		            System.out.println(line.p1.id + " " + line.p2.id );
		        }
		        
		
		        in.close();    	
	    	}catch(Exception e){e.printStackTrace();}
	      }
    }
}


