
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s124392
 */
public class Point {
    // Instance Variables.
    public int id;
    public float x;
    public float y;
    public boolean visited = false;
    public ArrayList<Line> lines = new ArrayList<Line>();
    
    public Point(int i, float x, float y){
        this.id = i;
        this.x = x;
        this.y = y;
    }
    
    public int degree(){
        return lines.size();
    }
    
}
