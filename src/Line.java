/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s124392
 */
public class Line {
    // Instance Variables.
    public Point p1;
    public Point p2;
    
    public Line(Point a, Point b){
        p1 = a;
        p2 = b;
    }
    
    public float length(){
        return (float) Math.sqrt( (p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));
    }
}
