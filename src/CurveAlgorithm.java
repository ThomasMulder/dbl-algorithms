import java.util.ArrayList;


public class CurveAlgorithm extends YOLOGraph {
    static ArrayList<Point> convexHull = new ArrayList<Point>();
    public static void run(){
    	 fixcurvehull();
         addpoints();
         findOpen();
    }
    public static void fixcurvehull(){
        
    	//find the most left point in the list of points
        Point mostleftpoint = points.get(0);
        for(Point point:points){
            if(point.x<mostleftpoint.x){
                mostleftpoint = point;
            }
        }
        
        //add the most left point to the convex hull
        convexHull.add(mostleftpoint);
        mostleftpoint.visited=true;
        
        
        Point lastPoint = mostleftpoint; 						//set the most left point as last point added to the convex hull
        Point nextPoint=notvisitedpoint();						//find a point that is not visited yet
        Point nextPoint2;
        
        boolean finish=true;
        while(!(nextPoint==null)&& finish ){					//loop while there is a point that is not visited yet.
            
            nextPoint.visited=true;								//set the point as visited
            nextPoint2=notvisitedpoint();						//find another point to check
            while(!(nextPoint2==null)){							//loop while there is another point that is not visited 
                nextPoint2.visited=true;						//set the second point as visited
                if(angle(nextPoint,lastPoint,nextPoint2)>0){	//compare angles between the last point and the two find points
                    nextPoint=nextPoint2;						//set the second point as first point when the angle is positive
                }
                nextPoint2=notvisitedpoint();					//go on with another not visited point
            }
            convexHull.add(nextPoint);							//add the point found to the convex hull
            lastPoint=nextPoint;								//set it as the last point
            if(lastPoint==mostleftpoint){finish=false;}			//if it is the first point in the convex hull then it is complete
            
            //reset the visited properties of the points.
            //only the points in the convex hull are visited
            for(Point point:points){
                point.visited=false;
            }
            for(Point point:convexHull){
                point.visited=true;
            }
            mostleftpoint.visited=false;
            
          nextPoint=notvisitedpoint(); 							//find the next point in the convexhull
        }
        
        //connect all the points in the convex hull
        for(int i=0;i<convexHull.size()-1;i++){
            connect(convexHull.get(i),convexHull.get(i+1));
          }
    }
    
    
    public static void addpoints(){
    	
    	//set all the points in the convex hull as visited
        for(Point point:convexHull){
            point.visited=true;
        }
        
        //find a point that is not in the convex hull
        Point nextPoint=notvisitedpoint();
        Point lastPoint=nextPoint;
        Line lastLine=lines.get(0);
        double dist=1.9001;
        double lastAngle=10;
        //loop through all not visited points
        while(!(nextPoint==null)){
            
        	// find the distance of the point to the closest line
            for(Line l:lines){
            	Point p1 =l.p1;
                Point p2 =l.p2;
                if(dist(nextPoint,l.p2)>dist(nextPoint,l.p1)){
                	p1 = l.p2;
                	p2 = l.p1;
                }
                double angle=vectorAngle(new Vector(nextPoint,p2),new Vector(p1,p2));
                
                
                if(distanceLineSegmentToPoint(nextPoint,l)<dist){
                    dist=distanceLineSegmentToPoint(nextPoint,l);
                    lastPoint=nextPoint;
                    lastLine=l;
                   
                    lastAngle=angle;
                }
                if(distanceLineSegmentToPoint(nextPoint,l)==dist && lastAngle>angle){
                    dist=distanceLineSegmentToPoint(nextPoint,l);
                    lastPoint=nextPoint;
                    lastLine=l;
                }
                
            }
            nextPoint.visited=true;
            nextPoint=notvisitedpoint();
        }
        
        //replace the line that is closest to a point by a line from both ends of that line to the point
        connect(lastPoint,lastLine.p1);
        connect(lastPoint,lastLine.p2);
        disconnect(lastLine);
        convexHull.add(lastPoint);
        for(Point point:points){
            point.visited=false;
        }
        for(Point point:convexHull){
            point.visited=true;
        }
        if(!(notvisitedpoint()==null)){
        	
            addpoints();
            
        	
        }
    }
       
    public static void findOpen(){
    	double maxLength=0;
    	double averageLength=0;
    	Line longestLine=null;
    	for(Line l:lines){
    		if(l.length()>maxLength){
    			maxLength=l.length();
    			longestLine=l;
    		}
    		averageLength=averageLength+l.length();
    	}
    	averageLength=averageLength/lines.size();
    	if(maxLength>2*averageLength){
    		disconnect(longestLine);
    	}
    }

}