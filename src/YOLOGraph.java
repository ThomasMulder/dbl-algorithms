
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s124392
 */
public class YOLOGraph {

    /**
     * @param args the command line arguments
     */
    static ArrayList<Point> points = new ArrayList<Point>();
    static ArrayList<Point> newPoints = new ArrayList<Point>();
    static ArrayList<Line> lines = new ArrayList<Line>();
    public static boolean multiple;
    public static boolean closed;
    public static boolean curve;
    public static boolean network;
    
    public static void main(String[] args){
    	if(!curve){
    		init();
    		CurveAlgorithm c = new CurveAlgorithm();
    		c.points = points;
    		c.run();
    		lines = c.lines;
    	}
    	new Printer().print(lines, points, network, true);
    }    
    

    public static void init(){
       Reader r = new Reader();
       r.read();
       points = r.points;
       multiple = r.multiple;
       curve = r.curve;
       network = r.network;
    }
    public static void connect(Point a, Point b){
    	
    	Line l = new Line(a,b);
    	if(!lines.contains(l)){
	        lines.add(l);
	        a.lines.add(l);
	        b.lines.add(l);
    	}
    }
    public static void disconnect(Line l){
        lines.remove(l);
        l.p1.lines.remove(l);
        l.p2.lines.remove(l);
    }
    public static Point newPoint(int i, float x, float y){
        Point p = new Point(i,x,y);
        newPoints.add(p);
        return p;
    }
    static double calculateAverageDensity(ArrayList<Point> p){
    	double totalDis = 0;
    	if(!multiple){
    		Point currentPoint = points.get(0);
    		currentPoint.visited = true;
    		while(!allPointsVisited(p)){
    			Point closestPoint = findClosestPoint(p,currentPoint);
    			closestPoint.visited = true;
    			totalDis += dist(closestPoint, currentPoint);
    			currentPoint = closestPoint;
    		}
    		for(Point point: p){
    			point.visited = false;
    		}
    	}
    	return totalDis/points.size();
    }
    static boolean allPointsVisited(ArrayList<Point> p){
    	for(Point point: p){
    		if(point.visited== false){
    			return false;
    		}
    	}
    	return true;
    }
    static Point findClosestPoint(ArrayList<Point> p, Point point){
    	@SuppressWarnings("unchecked")
		ArrayList<Point> points = (ArrayList<Point>) p.clone();
    	double distance = 1.9001;
    	Point tempPoint = points.get(0);
    	while(points.size() > 0){
    		if(points.get(0) != point){
	    		double tempdist = dist(point,points.get(0));
	    		if(tempdist < distance && !points.get(0).visited){
	    			distance = tempdist;
	    			tempPoint = points.get(0);
	    		}
	    	}
    		points.remove(0);
    	}
    	return tempPoint;
    }
    static double dist(Point p1, Point p2){
    	return (float) Math.sqrt( (p1.x-p2.x)*(p1.x-p2.x) + (p2.y-p2.y)*(p2.y-p2.y));
    }
    public static double angle(Point a, Point b, Point c){
        return ((a.x-b.x))*((c.y-b.y))-((a.y-b.y))*((c.x-b.x));
        
    }
    public static double distanceLineSegmentToPoint(Point a,Line l){
        Vector ap1 = new Vector(a ,l.p1);
        Vector ap2 = new Vector(a ,l.p2);
        Vector p1p2 = new Vector(l.p1 ,l.p2);
        Vector p2p1 = new Vector(l.p2 ,l.p1);
        
        
        if(vectorAngle(ap1,p2p1)>=Math.PI*0.5){
           return new Line(l.p1,a ).length();
        }
        if(vectorAngle(ap2,p1p2)>=Math.PI*0.5){
           return new Line(l.p2,a ).length();
        }
        double distance= new Line(l.p1,a).length();
        
        return distance*Math.sin(vectorAngle(ap1,p2p1));
        
    }
    public static Point notvisitedpoint(){
        for(Point point:points){
            if(point.visited==false){
                return point;
            }
        }
        return null;
    }
    public static float dotProduct(Vector a, Vector b){
    	return a.x * b.x + a.y * b.y;
    }
    public static float crossProduct(Vector a, Vector b){
    	return a.x * b.y - a.y * b.x;
    }
    public static float vectorAngle(Vector a, Vector b){
    	return (float)Math.acos(dotProduct(a,b)/(a.length()*b.length()));
    }
    public static float pointVectorAngle(Point a, Point b, Point c){
    	return 1;
    }
}
