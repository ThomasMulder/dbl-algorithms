import java.util.ArrayList;

public class NetworkAlgorithm extends YOLOGraph {

	
        static void run() {
        		double margin=0;
                int listSize = points.size() + 1;
               
                for(int i=1; i < listSize; i++) {
                        for(int j=2; i < listSize; i++) {
                                for(int k=3; i < listSize; i++) {
                                        if(i==j || j==k || i==k) {
                                                continue;                                       // the 2 vectors are the same
                                        }
                                       
                                        double angle = pointVectorAngle(points.get(i), points.get(j), points.get(k));
                                        if(angle > 180.0-margin || angle < 180.0+margin) {
                                                connect(points.get(i),points.get(j));connect(points.get(j),points.get(k));
                                        }
       
                                        if(angle > 0.0-margin || angle < 0.0+margin) {
                                                if(dist(points.get(i),points.get(j)) < dist(points.get(j),points.get(k))) {
                                                        connect(points.get(i),points.get(j));connect(points.get(i),points.get(k));
                                                } else {
                                                        connect(points.get(i),points.get(k));connect(points.get(j),points.get(k));                                                     
                                                }
                                        }
                                }
                        }
                }
        }
}